const mysql=require("mysql2");

const openconnection =mysql.createPool({
    host: 'demodb',
    user: 'root',
    password:'root',
    database: 'moviedb',
    port: 3306,
    waitForConnections: true,
    connectionLimit:10,
    queueLimit:0,
});

module.exports =openconnection