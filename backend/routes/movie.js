const express = require('express')
const db =require('../db')
const utils=require('../utils')

const router = express.Router();

router.get('/',(request,response)=>{
    const {movie_title}=request.body
    const query=`select * from movie where movie_title='${movie_title}'`;
    db.execute(query,(error,data)=>{
        response.send(utils.createResult(error,data))
    }) 
})

router.post('/add',(req,res)=>{
    const{movie_title, movie_release_date,movie_time,director_name}=req.body
    const stat=`insert into movie (movie_title, movie_release_date,movie_time,director_name) values('${movie_title}', '${movie_release_date}' ,'${movie_time}', '${director_name}')`;
    db.execute(stat,(error,data)=>{
        res.send(utils.createResult(error,data))
    })
})

router.put('/update',(req,res)=>{
    const{movie_id, movie_release_date,movie_time}=req.body
    const stat=
    `update movie 
    set movie_release_date='${movie_release_date}',
    movie_time='${movie_time}' 
    where movie_id='${movie_id}'`;
    db.execute(stat,(error,data)=>{
        res.send(utils.createResult(error,data))
    })
})

router.delete('/delete',(req,res)=>{
    const{movie_id}=req.body
    const stat=`delete from movie where movie_id='${movie_id}'`;
    db.execute(stat,(error,data)=>{
        res.send(utils.createResult(error,data))
    })
})


module.exports=router;