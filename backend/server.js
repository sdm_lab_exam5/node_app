const express=require("express")

const app=express();
const routeMovie= require('./routes/movie')

app.use(express.json())
app.use('/movie',routeMovie)


app.listen(4000,'0.0.0.0',()=>{
    console.log('server started on port no 4000')
})
