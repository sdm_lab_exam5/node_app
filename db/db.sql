
create table movie(
    movie_id int primary key auto_increment,
    movie_title varchar(200),
    movie_release_date varchar(100),
    movie_time varchar(100),
    director_name varchar(100)
);

insert into 
movie (movie_title, movie_release_date, movie_time, director_name) 
values('WAR','2020-09-07','23:00','Karan');